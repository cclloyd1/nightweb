# NightWeb - collection of dark themes for the web


## Quick install buttons:
- Myth-Weavers: [Install with Stylus](https://gitlab.com/cclloyd1/nightweb/-/raw/master/mythweavers/mythweavers.user.styl)
- Roll20: [Install with Stylus](https://gitlab.com/cclloyd1/nightweb/-/raw/master/roll20/roll20.user.styl)
- d20PFSRD: [Install with Stylus](https://gitlab.com/cclloyd1/nightweb/-/raw/master/d20pfsrd/d20pfsrd.user.styl)

